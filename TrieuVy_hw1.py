s="Today is a nice day , So we can go anywhere"
print('a. Chuỗi ban đầu: ',s)
print('b. Số ký tự trong chuỗi: ',len(s))
print('c. Xuất từ vị trí -5 đến -9:',s[-5:-9])
print('d. Xuất toàn bộ chuỗi với bước nhảy là 3:',s[::3])
s1 = s[:19]
s2 = s[22:]
print('e.Chuỗi S1 =',s1)
print('f.Chuỗi S2 =',s2)
s3 = s1 + s2
print('g.Chuỗi sau khi được hợp nhất và sắp xếp a-z :'," ".join(sorted(s3)))
print('h.Chuỗi sau khi được hợp nhất và in hoa: ',s3.upper())





