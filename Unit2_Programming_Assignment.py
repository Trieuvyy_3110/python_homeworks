def new_lines():
    print('.')

def three_lines():
    print(',')
    print(',')
    print(',')

def nine_lines():
    for i in range(5):
        print(three_lines())

def clear_screen():
    print(new_lines())
    for i in range(2):
        print(nine_lines())
    a = three_lines()
    del(a)
    print(three_lines())
